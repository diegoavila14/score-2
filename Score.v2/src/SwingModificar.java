import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

//esta clase representa la ventana que permite modificar caracter�sticas del objeto.
public class SwingModificar extends JFrame implements ActionListener {

	public MyObject objeto;
	public JPanel PanelVentanaJuego;
	public JLabel txtInforme;
	
	public JPanel myPanel;
	public JButton botonAcceptar;
	public JButton botonCancelar;
	private JTextField txtAngulo;
	private JTextField txtVelocidad;
	private JTextField txtMasa;
	private JTextField txtPosX;
	private JTextField txtPosY;
	
	
	public SwingModificar(MyObject objeto, JLabel txtInforme, JPanel panel)
	{
		setResizable( false );
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		this.objeto = objeto;
		this.txtInforme = txtInforme;
		this.PanelVentanaJuego = panel;
		
		myPanel = new JPanel();
		myPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		JLabel labelAngulo = new JLabel("�ngulo:");
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelAngulo, gbc);
		
		txtAngulo = new JTextField(5);
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtAngulo, gbc);
		
		JLabel labelAnguloUnidad = new JLabel("grados");
		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelAnguloUnidad, gbc);
		
		JLabel labelGravedad = new JLabel("Gravedad:");
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelGravedad, gbc);
		
		JTextField txtGravedad = new JTextField(5);
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtGravedad, gbc);
		
		JLabel labelGravedadUnidad = new JLabel("m/s�");
		gbc.gridx = 2;
		gbc.gridy = 1;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelGravedadUnidad, gbc);
		
		JLabel labelMasa = new JLabel("Masa:");
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelMasa, gbc);
		
		txtMasa = new JTextField(5);
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtMasa, gbc);
		
		JLabel labelMasaUnidad = new JLabel("kg");
		gbc.gridx = 2;
		gbc.gridy = 2;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelMasaUnidad, gbc);
		
		JLabel labelPosX = new JLabel("Posici�n en X:");
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelPosX, gbc);
		
		txtPosX = new JTextField(5);
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtPosX, gbc);
		
		JLabel labelPosY = new JLabel("Posici�n en Y:");
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelPosY, gbc);
		
		txtPosY = new JTextField(5);
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtPosY, gbc);
		
		JLabel labelVelocidad = new JLabel("Velocidad:");
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelVelocidad, gbc);
		
		txtVelocidad = new JTextField(5);
		gbc.gridx = 1;
		gbc.gridy = 5;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtVelocidad, gbc);
		
		JLabel labelVelocidadUnidad = new JLabel("m/s�");
		gbc.gridx = 2;
		gbc.gridy = 5;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelVelocidadUnidad, gbc);
		
		
		botonAcceptar = new JButton("Acceptar");
		botonAcceptar.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 7;
		gbc.insets = new Insets(20, 10, 10, 10);
		myPanel.add(botonAcceptar, gbc);
		
		botonCancelar = new JButton("Cancelar");
		botonCancelar.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 7;
		gbc.insets = new Insets(20, 10, 10, 10);
		myPanel.add(botonCancelar, gbc);
		
		this.add(myPanel);
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent event)
	{
		if(event.getSource() == botonAcceptar)
		{
			String mensajeError;
			
			if(verificarFormato(txtAngulo.getText()))
			{
				objeto.angle = Integer.parseInt(txtAngulo.getText());
			}
			
			else mensajeError = "No se ha podido modificar la variable �ngulo (problema con el formato ingresado).";
			
			GridBagConstraints gbc = new GridBagConstraints();
			txtInforme.setText(objeto.informeCaracteristicas());
			gbc = new GridBagConstraints();
			gbc.gridx = 10;
			gbc.gridy = 0;
			gbc.insets = new Insets(0, 50, 0, 50);
			myPanel.add(PanelVentanaJuego, gbc);
			
			this.dispose();
			}
		
		else
		{
			this.dispose();
		}
	}
	

//adaptado a partir de: http://stackoverflow.com/questions/8391979/does-java-have-a-int-tryparse-that-doesnt-throw-an-exception-for-bad-data
	

/**
 * m�todo que sirve para saber si el formato es adecuado (es entero)
 * @param txt, min, max
 * @return true si se puede convertir a un entero, false en caso contrario
 */
public boolean verificarFormato(String txt)
{
	try
	{
		Integer.parseInt(txt);
		return true;
	}
	
	catch (NumberFormatException nfe)
	{
		return false;
	}
	
}

	/**
	 * m�todo que sirve para saber si el formato es adecuado (es entero, dentro del rango deseado)
	 * @param txt, min, max
	 * @return true si se puede convertir a un entero y si est� dentro del rango deseado, false en caso contrario
	 */
	public boolean verificarFormato(String txt, int min, int max)
	{
		try
		{
			Integer.parseInt(txt);
			
			if(Integer.parseInt(txt)>=min && Integer.parseInt(txt)<=max)
			{
			return true;
			}
			
			else return false;
		}
		
		catch (NumberFormatException nfe)
		{
			return false;
		}
		
	}
	
	
	//fuente: http://stackoverflow.com/questions/144892/how-to-center-a-window-in-java
	
	/**
	 * este m�todo permite centrar las ventanas en la pantalla del computador
	 */
	public void centreWindow() {
	    Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
	    int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
	    this.setLocation(x, y);
	}
	
}
